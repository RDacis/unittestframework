using static UnitTestFrameworks.MaxConsecutive;

namespace UnitTestFrameworks.Tests
{
    [TestClass]
    public class MaxConsecutiveTests
    {
        [TestMethod]
        public void UnequalCharacters_WithNullValue_ReturnsZero()
        {
            // Arrange
            string? userInput = null;

            // Act
            var actual = UnequalCharacters(userInput);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void UnequalCharacters_WithEmptyString_ReturnsZero()
        {
            // Arrange
            string userInput = string.Empty;

            // Act
            var actual = UnequalCharacters(userInput);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void UnequalCharacters_WithWhitespaceCharacter_ReturnsOne()
        {
            // Arrange
            string userInput = " ";

            // Act
            var actual = UnequalCharacters(userInput);

            // Assert
            Assert.AreEqual(1, actual);
        }

        [TestMethod]
        [DataRow("a")]
        [DataRow("1")]
        public void UnequalCharacters_WithSingleCharacter_ReturnsOne(string userInput)
        {
            // Arrange

            // Act
            var actual = UnequalCharacters(userInput);

            // Assert
            Assert.AreEqual(1, actual);
        }

        [TestMethod]
        [DataRow("aa", 1)]
        [DataRow("aab", 2)]
        [DataRow("Test", 4)]
        [DataRow("Tennessee", 3)]
        [DataRow("Mississippi", 3)]
        [DataRow("A 1ong sentenc3.", 16)]

        public void UnequalCharacters_WithMultipleCharacters_ReturnsCorrectValue(string userInput, int expected)
        {
            // Arrange

            // Act
            var actual = UnequalCharacters(userInput);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IdenticalLetters_WithNullValue_ReturnsZero()
        {
            // Arrange
            string? userInput = null;

            // Act
            var actual = IdenticalLetters(userInput);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void IdenticalLetters_WithEmptyString_ReturnsZero()
        {
            // Arrange
            string userInput = string.Empty;

            // Act
            var actual = IdenticalLetters(userInput);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void IdenticalLetters_WithWhitespaceCharacter_ReturnsZero()
        {
            // Arrange
            string userInput = " ";

            // Act
            var actual = IdenticalLetters(userInput);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [DataRow("1", 0)]
        [DataRow("@", 0)]
        [DataRow("A", 1)]
        [DataRow("B", 1)]
        [DataRow("Y", 1)]
        [DataRow("Z", 1)]
        [DataRow("[", 0)]
        [DataRow("`", 0)]
        [DataRow("a", 1)]
        [DataRow("b", 1)]
        [DataRow("y", 1)]
        [DataRow("z", 1)]
        [DataRow("{", 0)]
        public void IdenticalLetters_WithSingleCharacter_ReturnsCorrectValue(string userInput, int expected)
        {
            // Arrange

            // Act
            var actual = IdenticalLetters(userInput);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("Test", 1)]
        [DataRow("Tennesseee", 3)]
        [DataRow("Looooonnnnng", 5)]
        [DataRow("...   123 aa ", 2)]

        public void IdenticalLetters_WithMultipleCharacters_ReturnsCorrectValue(string input, int expected)
        {
            // Arrange

            // Act
            var actual = IdenticalLetters(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IdenticalNumbers_WithNullValue_ReturnsZero()
        {
            // Arrange
            string? userInput = null;

            // Act
            var actual = IdenticalNumbers(userInput);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void IdenticalNumbers_WithEmptyString_ReturnsZero()
        {
            // Arrange
            string userInput = string.Empty;

            // Act
            var actual = IdenticalNumbers(userInput);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void IdenticalNumbers_WithWhitespaceCharacter_ReturnsZero()
        {
            // Arrange
            string userInput = " ";

            // Act
            var actual = IdenticalNumbers(userInput);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [DataRow("a", 0)]
        [DataRow("/", 0)]
        [DataRow("0", 1)]
        [DataRow("1", 1)]
        [DataRow("8", 1)]
        [DataRow("9", 1)]
        [DataRow(":", 0)]
        public void IdenticalNumbers_WithSingleCharacter_ReturnsCorrectValue(string userInput, int expected)
        {
            // Arrange

            // Act
            var actual = IdenticalNumbers(userInput);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("Test", 0)]
        [DataRow("3Tennessee3.3", 1)]
        [DataRow("122333444455555666656", 5)]
        [DataRow("This happened at least 2 000 000 or 2.000000 or 2.000_000 years ago.", 6)]

        public void IdenticalNumbers_WithMultipleCharacters_ReturnsCorrectValue(string input, int expected)
        {
            // Arrange

            // Act
            var actual = IdenticalNumbers(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
