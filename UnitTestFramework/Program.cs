﻿using UnitTestFrameworks;

Console.Write("Please input some text: ");
string? userInput = Console.ReadLine();
Console.WriteLine($"Maximum number of unequal consecutive characters in the provided string - {MaxConsecutive.UnequalCharacters(userInput)}");
Console.WriteLine($"Maximum number of consecutive identical letters of the Latin alphabet in the provided string - {MaxConsecutive.IdenticalLetters(userInput)}");
Console.WriteLine($"Maximum number of consecutive identical digits in the provided string - {MaxConsecutive.IdenticalNumbers(userInput)}");
