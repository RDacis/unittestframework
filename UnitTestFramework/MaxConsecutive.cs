﻿namespace UnitTestFrameworks
{
    public static class MaxConsecutive
    {
        public static int UnequalCharacters(string? userInput)
        {
            if (string.IsNullOrEmpty(userInput))
            {
                return 0;
            }

            int count = 1, maxConsecutive = 1;
            for (var i = 1; i < userInput.Length; i++)
            {
                if (userInput[i] != userInput[i - 1])
                {
                    if (maxConsecutive < ++count)
                    {
                        maxConsecutive = count;
                    }
                }
                else
                {
                    count = 1;
                }
            }

            return maxConsecutive;
        }

        public static int IdenticalLetters(string? userInput)
        {
            if (string.IsNullOrWhiteSpace(userInput) || !userInput.Any(char.IsAsciiLetter))
            {
                return 0;
            }

            int count = 1, maxConsecutive = 1;
            for (var i = 1; i < userInput.Length; i++)
            {
                if (char.IsAsciiLetter(userInput[i]) && char.IsAsciiLetter(userInput[i - 1]) && (userInput[i] == char.ToLowerInvariant(userInput[i - 1]) || userInput[i] == char.ToUpperInvariant(userInput[i - 1])))
                {
                    if (maxConsecutive < ++count)
                    {
                        maxConsecutive = count;
                    }
                }
                else
                {
                    count = 1;
                }
            }

            return maxConsecutive;
        }

        public static int IdenticalNumbers(string? userInput)
        {
            if (string.IsNullOrWhiteSpace(userInput) || !userInput.Any(char.IsAsciiDigit))
            {
                return 0;
            }

            int count = 1, maxConsecutive = 1;
            for (var i = 1; i < userInput.Length; i++)
            {
                if (char.IsDigit(userInput[i]) && char.IsDigit(userInput[i - 1]) && userInput[i] == userInput[i - 1])
                {
                    if (maxConsecutive < ++count)
                    {
                        maxConsecutive = count;
                    }
                }
                else
                {
                    count = 1;
                }
            }

            return maxConsecutive;
        }
    }
}
